//#include <HCSR04.h>

// Definiowanie pinów dla pierwszego czujnika
byte triggerPin = 9;
byte echoPin = 12;

// Definiowanie pinów dla drugiego czujnika
byte triggerPin2 = 5;
byte echoPin2 = 8;

// Definiowanie pinów dla trzeciego czujnika
byte triggerPin3 = 4;
byte echoPin3 = 3;

// Ustawienia początkowe
void setup() {
  Serial.begin(9600); // Inicjowanie monitora szeregowego z prędkością 9600 bitów na sekundę
  pinMode(triggerPin, OUTPUT); // Ustawienie pinu trigger jako wyjście
  pinMode(echoPin, INPUT); // Ustawienie pinu echo jako wejście
  pinMode(triggerPin2, OUTPUT); // Ustawienie pinu trigger2 jako wyjście
  pinMode(echoPin2, INPUT); // Ustawienie pinu echo2 jako wejście
  pinMode(triggerPin3, OUTPUT); // Ustawienie pinu trigger3 jako wyjście
  pinMode(echoPin3, INPUT); // Ustawienie pinu echo3 jako wejście
}

// Funkcja do odczytu odległości z czujnika
float readDistance(byte trigger, byte echo) {
  digitalWrite(trigger, LOW); // Ustawienie pinu trigger jako stan niski
  delayMicroseconds(2); // Oczekiwanie 2 mikrosekundy
  digitalWrite(trigger, HIGH); // Ustawienie pinu trigger jako stan wysoki
  delayMicroseconds(10); // Oczekiwanie 10 mikrosekund
  digitalWrite(trigger, LOW); // Ustawienie pinu trigger jako stan niski

  float duration = pulseIn(echo, HIGH); // Odczytanie czasu trwania sygnału ultradźwiękowego
  float distance = duration * 0.034 / 2; // Obliczenie odległości na podstawie czasu trwania sygnału ultradźwiękowego
  
  return distance; // Zwrócenie odległości
}

// Pętla główna
void loop() {
  // Odczytanie odległości z pierwszego czujnika
  float distance1 = readDistance(triggerPin, echoPin);
  Serial.print("Distance 1: ");
  Serial.print(distance1);
  Serial.println(" cm");

  // Odczytanie odległości z drugiego czujnika
  float distance2 = readDistance(triggerPin2, echoPin2);
  Serial.print("Distance 2: ");
  Serial.print(distance2);
  Serial.println(" cm");

  // Odczytanie odległości z trzeciego czujnika
  float distance3 = readDistance(triggerPin3, echoPin3);
  Serial.print("Distance 3: ");
  Serial.print(distance3);
  Serial.println(" cm");

  delay(500); // Oczekiwanie 500 milisekund przed kolejnym odczytem
}
