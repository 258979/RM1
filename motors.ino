#define A1A 6
#define A1B 7
#define B1A 10
#define B1B 11

void setup() {
  pinMode(B1A,OUTPUT);
  pinMode(B1B,OUTPUT);
  
  pinMode(A1A,OUTPUT);
  pinMode(A1B,OUTPUT);    
  delay(3000);
}

void loop() {

  motorA('R');// do przodu  
  motorB('L');// do przodu  
  delay(3000);
  motorA('O');// wył
  motorB('O');// wył
  delay(5000);
   
}

void motorA(char d)
{
  if(d =='R'){
    digitalWrite(A1A,LOW);
    digitalWrite(A1B,HIGH); 
  }else if (d =='L'){
    digitalWrite(A1A,HIGH);
    digitalWrite(A1B,LOW);    
  }else{
    digitalWrite(A1A,LOW);
    digitalWrite(A1B,LOW);    
  }
}

void motorB(char d)
{

    if(d =='R'){
      digitalWrite(B1A,LOW);
      digitalWrite(B1B,HIGH); 
    }else if(d =='L'){
      digitalWrite(B1A,HIGH);
      digitalWrite(B1B,LOW);    
    }else{    
      digitalWrite(B1A,LOW);
      digitalWrite(B1B,LOW);     
    }

}
