#include <Servo.h>

Servo myservo;

int pos = 0;

void setup() {
  myservo.attach(2);
}

void loop() {
  for (pos = 20; pos <= 160; pos += 1) {
    myservo.write(pos);
    delay(10);
  }
  for (pos = 160; pos >= 20; pos -= 1) {
    myservo.write(pos);
    delay(10);
  }
}
