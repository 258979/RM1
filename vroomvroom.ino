#include <Servo.h>
#include <HCSR04.h>

// Servo Pin
#define SERVO_PIN 2

// Left Motor Pins
#define A1A 6
#define A1B 7

// Right Motor Pins
#define B1A 10
#define B1B 11

// Distance Sensors
byte triggerPin = 3;
byte echoCount = 3;
byte echoPins[] = {4, 5, 8};

// Minimum distance to obstacle (in centimeters)
#define MIN_DISTANCE 20

Servo myservo;

// Car movement states
enum CarState {
  FORWARD,
  BACKWARD,
  TURN_LEFT,
  TURN_RIGHT,
  STOP
};

CarState carState = STOP;  // Initial car state
unsigned long startTime = 0;  // Program start time

// Setup of peripherials
void setup() {
  HCSR04.begin(triggerPin, echoPins, echoCount);
  pinMode(A1A, OUTPUT);
  pinMode(A1B, OUTPUT);
  pinMode(B1A, OUTPUT);
  pinMode(B1B, OUTPUT);

  myservo.attach(SERVO_PIN);

  Serial.begin(9600);

  stopCar();

  startTime = millis();
}

void moveForward() {
  myservo.write(90);
  digitalWrite(A1A, LOW);
  digitalWrite(A1B, HIGH);
  digitalWrite(B1A, HIGH);
  digitalWrite(B1B, LOW);
}

void moveBackward() {
  myservo.write(130);
  digitalWrite(A1A, HIGH);
  digitalWrite(A1B, LOW);
  digitalWrite(B1A, LOW);
  digitalWrite(B1B, HIGH);
  delay(500);
}

void turnLeft() {
  myservo.write(130);
  digitalWrite(A1A, LOW);
  digitalWrite(A1B, HIGH);
  digitalWrite(B1A, LOW);
  digitalWrite(B1B, LOW);
  delay(300);
}

void turnRight() {
  myservo.write(50);
  digitalWrite(A1A, LOW);
  digitalWrite(A1B, LOW);
  digitalWrite(B1A, HIGH);
  digitalWrite(B1B, LOW);
  delay(300);
}

void stopCar() {
  digitalWrite(A1A, LOW);
  digitalWrite(A1B, LOW);
  digitalWrite(B1A, LOW);
  digitalWrite(B1B, LOW);
}

// Main loop
void loop() {
  if (millis() - startTime >= 5000) {
    carState = FORWARD;
  }

  double* distances = HCSR04.measureDistanceCm();

  float distanceLeft = distances[2];
  float distanceCenter = distances[1];
  float distanceRight = distances[0];

  if (carState == FORWARD) {
    if (distanceCenter < MIN_DISTANCE) {
      carState = BACKWARD;
    } else if (distanceLeft < MIN_DISTANCE) {
      carState = TURN_RIGHT;
    } else if (distanceRight < MIN_DISTANCE) {
      carState = TURN_LEFT;
    }
  } else if (carState == BACKWARD) {
    if (millis() - startTime >= 1000) {
      carState = FORWARD;
    }
  } else if (carState == TURN_LEFT) {
    if (millis() - startTime >= 1000) {
      carState = BACKWARD;
    }
  } else if (carState == TURN_RIGHT) {
    if (millis() - startTime >= 1000) {
      carState = BACKWARD;
    }
  }
  
  if (carState == FORWARD) {
    moveForward();
  } else if (carState == BACKWARD) {
    moveBackward();
  } else if (carState == TURN_LEFT) {
    turnLeft();
  } else if (carState == TURN_RIGHT) {
    turnRight();
  } else {
    stopCar();
  }

  // Print distance readings for each sensor
  for (int i = 0; i < echoCount; i++) {
    Serial.print("Sensor ");
    Serial.print(i + 1);
    Serial.print(": ");
    Serial.print(distances[i]);
    Serial.println(" cm");
  }
}
